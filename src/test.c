#define _DEFAULT_SOURCE

#include "test.h"

#include <unistd.h>
#include <stdio.h>

#include "mem_internals.h"
#include "mem.h"
#include "mem_debug.h"
#include "util.h"


#define HEAP_SIZE 10000

static struct block_header* get_block_from_data(void* data) {
    return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}

void* test0(){
    debug("Инициализируем кучу размера 10000...\n");
    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL) {
        err("Инициализация кучи провалена\n");
    } else {
        debug("Куча успешно инициализирована\n");
    }
    return heap;
}

void test1(struct block_header *first_block) {
    debug("Тест 1: Обычное выделение памяти с помощью _malloc\n");

    void *data = _malloc(100);

    if (data == NULL) {
        err("Тест 1 провален: _malloc вернул null\n");
    }
    debug_heap(stdout, first_block);

    if (first_block->is_free) {
        err("Тест 1 провален: is_free определен некорректно\n");
    }

    if (first_block->capacity.bytes != 100) {
        err("Тест 1 провален: capacity определена некорректно\n");
    }

    _free(data);

    debug("Тест 1 пройден\n");
}

void test2(struct block_header *first_block) {
    debug("Тест 2: Освобождение одного блока из нескольких выделенных\n");

    void *data1 = _malloc(100);
    void *data2 = _malloc(200);

    if (data1 == NULL || data2 == NULL) {
        err("Тест 2 провален: _malloc вернул null для какого то блока\n");
    }

    debug_heap(stdout, first_block);
    _free(data1);
    debug_heap(stdout, first_block);

    struct block_header *data1_block = get_block_from_data(data1);
    struct block_header *data2_block = get_block_from_data(data2);
    if (!data1_block->is_free) {
        err("Тест 2 провален: блок 1 не освобожден\n");
    }
    if (data2_block->is_free) {
        err("Тест 2 провален: блок 2 не должен быть освобожден\n");
    }
    debug("Tест 2 пройден\n");
    _free(data2);
}

void test3 (struct block_header *first_block) {
    debug("Тест 3: Освобождение двух блоков из нескольких выделенных.\n");

    void *data1 = _malloc(1000);
    void *data2 = _malloc(1000);
    void *data3 = _malloc(1000);

    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Тест 3 провален: _malloc вернул null для какого то блока.\n");
    }

    debug_heap(stdout, first_block);
    _free(data2);
    debug_heap(stdout, first_block);
    _free(data1);
    debug_heap(stdout, first_block);

    struct block_header *data1_header = get_block_from_data(data1);
    struct block_header *data2_header = get_block_from_data(data2);
    struct block_header *data3_header = get_block_from_data(data3);
    if (!data1_header->is_free) {
        err("Тест 3 провален: блок 1 не освобожден\n");
    }
    if (!data2_header->is_free) {
        err("Тест 3 провален: блок 2 не освобожден\n");
    }
    if (data3_header->is_free) {
        err("Тест 3 провален: блок 3 не должен быть освобожден\n");
    }
    if (data1_header->capacity.bytes != 2000+offsetof(struct block_header, contents)) {
        err("Тест 3 провален: освобожденные рядом блоки не объединились\n");
    }
    debug("Тест 3 пройден\n");
    _free(data1);
    _free(data2);
    _free(data3);
}

void test4(struct block_header *first_block) {
    debug("Тест 4: Память закончилась, новый регион памяти расширяет старый.\n");

    void *data1 = _malloc(HEAP_SIZE);
    debug_heap(stdout, first_block);
    void *data2 = _malloc(HEAP_SIZE);
    debug_heap(stdout, first_block);

    if (data1 == NULL || data2 == NULL) {
        err("Тест 4 провален: malloc вернул null для какого-то блока\n");
    }

    struct block_header *data1_block = get_block_from_data(data1);
    struct block_header *data2_block = get_block_from_data(data2);

    if ((uint8_t*) data1_block->contents + data1_block->capacity.bytes != (uint8_t*) data2_block){
        err("Тест 4 провален: блок 2 был выделен не саразу после\n");
    }
    debug("Тест 4 пройден\n");
    _free(data1);
    _free(data2);
}

void test5(struct block_header *first_block) {
    debug("Тест 5: Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.\n");
    
    void *data1 = _malloc(HEAP_SIZE);
    if (data1 == NULL) {
        err("Тест 5 провален: _malloc вернул null для блока 1\n");
    }

    struct block_header *start = first_block;
    while (start->next != NULL) start = start->next;

    map_pages((uint8_t *) start+size_from_capacity(start->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void *data2 = _malloc(HEAP_SIZE*10);
    if (data2 == NULL) {
        err("Тест 5 провален: _malloc вернул null для блока 2\n");
    }
    debug_heap(stdout, first_block);

    struct block_header *data2_block = get_block_from_data(data2);
    if (data2_block == start) {
        err("Тест 5 провален: блок выделился рядом, но должен был в новом месте\n");
    }
    debug("Тест 5 пройден\n");
    _free(data1);
    _free(data2);
}

