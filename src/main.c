#include "test.h"
#include "mem_debug.h"

int main() {
    struct block_header* first_block = (struct block_header*) test0();
    if (first_block!= NULL) {
        test1(first_block);
        test2(first_block);
        test3(first_block);
        test4(first_block);
        test5(first_block);
        debug("Все тесты успешно пройдены\n");
        printf("\n");
    }
    return 0;
}


